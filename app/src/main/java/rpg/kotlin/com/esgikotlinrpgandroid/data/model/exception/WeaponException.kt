package rpg.kotlin.com.esgikotlinrpgandroid.data.model.exception

class WeaponException(override var message : String) : Exception()