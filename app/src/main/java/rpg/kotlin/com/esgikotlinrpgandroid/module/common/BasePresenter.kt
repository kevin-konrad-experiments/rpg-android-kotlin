package rpg.kotlin.com.esgikotlinrpgandroid.module.common

import android.os.Bundle
import android.view.View
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Monster
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Room

abstract class BasePresenter {
  open fun onCreated() = Unit
  open fun onResume() = Unit
  open fun onPause() = Unit
  open fun onStart() = Unit
  open fun onStop() = Unit
  open fun onDestroy() = Unit
  open fun onAttach() = Unit
  open fun onDetach() = Unit
  open fun onViewCreated(rootView: View, savedInstanceState: Bundle?) = Unit
  open fun useKeyToUnlockRoom(room: Room) = Unit
  open fun attackMonster(monster: Monster) = Unit
}