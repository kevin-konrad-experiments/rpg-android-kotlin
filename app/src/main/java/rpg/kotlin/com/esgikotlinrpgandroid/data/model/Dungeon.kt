package rpg.kotlin.com.esgikotlinrpgandroid.data.model

data class Dungeon(val name : String = "Kotlin", val rooms : Map<RoomName, Room>, var currentRoom: Room? = null)
