package rpg.kotlin.com.esgikotlinrpgandroid.module.inventory

import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player

interface InventoryInterface {
    fun displayInventory(player: Player?)
}