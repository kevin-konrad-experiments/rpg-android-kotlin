package rpg.kotlin.com.esgikotlinrpgandroid.module.map

import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Monster
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Room

interface MapInterface {
    fun displayRoom(room: Room)
    fun setLayoutFillParent()
    fun askPlayerChangeRoom(room: Room)
    fun askPlayerUseKey(room: Room)
    fun displayUsedKeyMessage(player: Player)
    fun displayNotEnoughKeysMessage()
    fun displayCannotChangeRoomWhileMonsterMessage()
    fun displayMonsterKilledMessage(monster: Monster)
}