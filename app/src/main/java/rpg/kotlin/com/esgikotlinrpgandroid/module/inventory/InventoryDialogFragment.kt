package rpg.kotlin.com.esgikotlinrpgandroid.module.inventory

import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player
import rpg.kotlin.com.esgikotlinrpgandroid.R
import rpg.kotlin.com.esgikotlinrpgandroid.module.common.BaseDialogFragment

class InventoryDialogFragment : BaseDialogFragment(R.layout.fragment_inventory), InventoryInterface {
    private val presenter = InventoryPresenter(this)

    override fun getPresenter() = presenter

    override fun displayInventory(player: Player?) {
        // TODO
    }
}