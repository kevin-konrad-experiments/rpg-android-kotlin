package rpg.kotlin.com.esgikotlinrpgandroid.module.common

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseDialogFragment constructor(@LayoutRes val layoutRes: Int) : DialogFragment() {

    protected abstract fun getPresenter(): BasePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getPresenter().onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        getPresenter().onAttach()
    }

    override fun onStart() {
        super.onStart()
        getPresenter().onStart()
    }

    override fun onDetach() {
        super.onDetach()
        getPresenter().onDetach()
    }

    override fun onResume() {
        super.onResume()
        getPresenter().onResume()
    }

    override fun onPause() {
        getPresenter().onPause()
        super.onPause()
    }

    override fun onStop() {
        getPresenter().onStop()
        super.onStop()
    }

    override fun onDestroy() {
        getPresenter().onDestroy()
        super.onDestroy()
    }
}
