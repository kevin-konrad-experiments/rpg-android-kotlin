package rpg.kotlin.com.esgikotlinrpgandroid.module.map

import android.os.Bundle
import android.view.View
import rpg.kotlin.com.esgikotlinrpgandroid.data.DataProvider
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Dungeon
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Monster
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Room
import rpg.kotlin.com.esgikotlinrpgandroid.module.common.BasePresenter

class MapPresenter(private val view: MapInterface) : BasePresenter() {
    private lateinit var dungeon: Dungeon
    private lateinit var player: Player

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        player = DataProvider.player
        dungeon = DataProvider.dungeon
        dungeon.currentRoom?.let { view.displayRoom(it) }
    }

    override fun onStart() {
        view.setLayoutFillParent()
    }

    fun onNorthRoomClick() {
        dungeon.currentRoom?.let {currentRoom ->
            currentRoom.northRoom?.let { northRoom -> onRoomClick(northRoom) }
        }
    }

    fun onSouthRoomClick() {
        dungeon.currentRoom?.let {currentRoom ->
            currentRoom.southRoom?.let { southRoom -> onRoomClick(southRoom) }
        }
    }

    fun onEastRoomClick() {
        dungeon.currentRoom?.let {currentRoom ->
            currentRoom.eastRoom?.let { eastRoom -> onRoomClick(eastRoom) }
        }
    }

    fun onWestRoomClick() {
        dungeon.currentRoom?.let {currentRoom ->
            currentRoom.westRoom?.let { westRoom -> onRoomClick(westRoom) }
        }
    }

    private fun onRoomClick(room: Room) {
        dungeon.currentRoom?.let { currentRoom ->
            if (currentRoom.monster != null && currentRoom.monster.healthPoint > 0) {
                view.displayCannotChangeRoomWhileMonsterMessage()
                return
            }

            if (room.hasLockDoor) {
                view.askPlayerUseKey(room)
            } else {
                view.askPlayerChangeRoom(room)
            }
        }
    }

    override fun attackMonster(monster: Monster) {
        dungeon.currentRoom?.let { currentRoom ->
            currentRoom.monster?.healthPoint = 0
            view.displayMonsterKilledMessage(monster)
            view.displayRoom(currentRoom)
        }
    }

    override fun useKeyToUnlockRoom(room: Room) {
        if (room.hasLockDoor && player.nbKey > 0) {
            room.hasLockDoor = false
            player.nbKey--
            view.displayUsedKeyMessage(player)
        } else if (player.nbKey <= 0) {
            view.displayNotEnoughKeysMessage()
        }
    }

    fun changeRoom(room: Room) {
        dungeon.currentRoom = room
        dungeon.currentRoom?.let { view.displayRoom(it) }
    }
}