package rpg.kotlin.com.esgikotlinrpgandroid.module.map

import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Monster
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.MonsterType
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Room
import kotlinx.android.synthetic.main.fragment_map.*
import rpg.kotlin.com.esgikotlinrpgandroid.R
import rpg.kotlin.com.esgikotlinrpgandroid.module.common.BaseDialogFragment

class MapDialogFragment : BaseDialogFragment(R.layout.fragment_map), MapInterface {
    private val presenter = MapPresenter(this)

    override fun getPresenter() = presenter

    override fun displayRoom(room: Room) {
        map_room_name_txv.text = room.name.roomName

        map_monster_btn.visibility = View.GONE
        room.monster?.let { monster ->
            if (monster.healthPoint > 0) {
                map_monster_btn.visibility = View.VISIBLE
                map_monster_btn.setOnClickListener { presenter.attackMonster(monster) }

                when(monster.type) {
                    MonsterType.DRAGON -> map_monster_btn.setImageResource(R.drawable.ic_dragon)
                    MonsterType.ORC -> map_monster_btn.setImageResource(R.drawable.ic_orc)
                    MonsterType.TROLL -> map_monster_btn.setImageResource(R.drawable.ic_troll)
                    MonsterType.GOBLIN -> map_monster_btn.setImageResource(R.drawable.ic_goblin)
                }
            }
        }

        map_room_north_btn.visibility = View.GONE
        map_room_north_txv.visibility = View.GONE
        room.northRoom?.let { northRoom ->
            map_room_north_txv.visibility = View.VISIBLE
            map_room_north_txv.text = northRoom.name.roomName
            map_room_north_btn.setImageResource(if (northRoom.hasLockDoor) R.drawable.ic_locked else R.drawable.ic_unlocked)
            map_room_north_btn.visibility = View.VISIBLE
            map_room_north_btn.setOnClickListener { presenter.onNorthRoomClick() }
        }

        map_room_south_btn.visibility = View.GONE
        map_room_south_txv.visibility = View.GONE
        room.southRoom?.let { southRoom ->
            map_room_south_txv.visibility = View.VISIBLE
            map_room_south_txv.text = southRoom.name.roomName
            map_room_south_btn.setImageResource(if (southRoom.hasLockDoor) R.drawable.ic_locked else R.drawable.ic_unlocked)
            map_room_south_btn.visibility = View.VISIBLE
            map_room_south_btn.setOnClickListener { presenter.onSouthRoomClick() }
        }

        map_room_east_btn.visibility = View.GONE
        map_room_east_txv.visibility = View.GONE
        room.eastRoom?.let { eastRoom ->
            map_room_east_txv.visibility = View.VISIBLE
            map_room_east_txv.text = eastRoom.name.roomName
            map_room_east_btn.setImageResource(if (eastRoom.hasLockDoor) R.drawable.ic_locked else R.drawable.ic_unlocked)
            map_room_east_btn.visibility = View.VISIBLE
            map_room_east_btn.setOnClickListener { presenter.onEastRoomClick() }
        }

        map_room_west_btn.visibility = View.GONE
        map_room_west_txv.visibility = View.GONE
        room.westRoom?.let { westRoom ->
            map_room_west_txv.visibility = View.VISIBLE
            map_room_west_txv.text = westRoom.name.roomName
            map_room_west_btn.setImageResource(if (westRoom.hasLockDoor) R.drawable.ic_locked else R.drawable.ic_unlocked)
            map_room_west_btn.visibility = View.VISIBLE
            map_room_west_btn.setOnClickListener { presenter.onWestRoomClick() }
        }
    }

    override fun setLayoutFillParent() {
        val width = (resources.displayMetrics.widthPixels * 0.95).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.95).toInt()

        dialog.window.setLayout(width, height)
    }

    override fun askPlayerUseKey(room: Room) {
        val message = "Voulez-vous utiliser une clé pour déverrouiller la pièce ${room.name.roomName} ?"

        AlertDialog.Builder(context!!)
            .setPositiveButton("Go") { _, _ -> presenter.useKeyToUnlockRoom(room)}
            .setNeutralButton("Non") { dialog, _ -> dialog.dismiss() }
            .setMessage(message)
            .create()
            .show()
    }

    override fun askPlayerChangeRoom(room: Room) {
        val message = "Voulez-vous vous déplacer vers la pièce ${room.name.roomName} ?"

        AlertDialog.Builder(context!!)
            .setPositiveButton("Go") { _, _ -> presenter.changeRoom(room)}
            .setNeutralButton("Non") { dialog, _ -> dialog.dismiss() }
            .setMessage(message)
            .create()
            .show()
    }

    override fun displayNotEnoughKeysMessage() {
        Toast.makeText(context, "Vous n'avez pas assez de clés !", Toast.LENGTH_SHORT).show()
    }

    override fun displayUsedKeyMessage(player: Player) {
        Toast.makeText(context, "Vous avez utilisé 1 clé. ${player.nbKey} restante(s)", Toast.LENGTH_SHORT).show()
    }

    override fun displayMonsterKilledMessage(monster: Monster) {
        Toast.makeText(context, "Bravo ! Vous avez défait un ${monster.type.typeName}.", Toast.LENGTH_SHORT).show()
    }

    override fun displayCannotChangeRoomWhileMonsterMessage() {
        Toast.makeText(context, "Vous ne pouvez pas changer de pièce en présence d'un monstre !", Toast.LENGTH_SHORT).show()
    }
}