package rpg.kotlin.com.esgikotlinrpgandroid.module.inventory

import rpg.kotlin.com.esgikotlinrpgandroid.data.DataProvider
import rpg.kotlin.com.esgikotlinrpgandroid.data.model.Player
import rpg.kotlin.com.esgikotlinrpgandroid.module.common.BasePresenter

class InventoryPresenter(private val view: InventoryInterface) : BasePresenter() {
    lateinit var player: Player

    override fun onCreated() {
        player = DataProvider.player
        view.displayInventory(player)
    }
}